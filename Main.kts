class Point(var x: Int, var y: Int) {
    override fun toString() : String {
        return "$x,$y"
    }

    override fun equals(other: Any?) : Boolean {
        if (other == null || other.javaClass != this.javaClass) {
            return false
        }
        val point = other as Point
        return this.x == point.x && this.y == point.y
    }

    fun sym() {
        this.x = -this.x;
        this.y = -this.y;
    }
}

var p : Point = Point(1, 2)
var p1 : Point = Point(1, 2)
var p2 : Point = Point(5, 3)
var p3 : Point = Point(-1, -2)

println(p.toString())
println(p.equals(p1))
println(p.equals(p2))
println(p.equals(p3))
p.sym()
println(p.toString())
println(p.equals(p3))